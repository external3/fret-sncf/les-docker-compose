In this document, links for all the files ".yml" used. There are some modifications added to fit in with the context of our project. 

Links to ".yml" files : 

- Gitlab: https://docs.gitlab.com/omnibus/docker/ 
- Jenkins: https://hub.docker.com/r/bitnami/jenkins/
- Nexus: https://github.com/sonatype/docker-nexus3
- SonarQube: https://github.com/SonarSource/docker-sonarqube/

